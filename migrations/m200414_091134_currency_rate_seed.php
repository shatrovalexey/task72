<?php

use yii\db\Migration;

/**
 * Class m200414_091134_currency_rate_seed
 */
class m200414_091134_currency_rate_seed extends Migration
{
	protected function getDocument( $url ) {
		$domh = new \DOMDocument( '1.0' , 'utf-8' ) ;
		$domh->load( $url ) ;
		return new DOMXPath( $domh ) ;
	}

    /**
     * {@inheritdoc}
     */
    public function safeUp( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$sth_ins = $dbh->prepare( "
INSERT IGNORE INTO
	`currency`
SET
	`id` := :id ,
	`name` := :name ,
	`char_code` := :char_code ,
	`num_code` := :num_code ,
	`nominal` := :nominal ,
	`parent_id` := :parent_id ;
	" ) ;

	foreach ( self::getDocument( 'http://www.cbr.ru/scripts/XML_valFull.asp' )->query( '/Valuta/Item' ) as $item
	) {
		$id = $item->getAttribute( 'ID' ) ;
		$name = $item->getElementsByTagName( 'EngName' )->item( 0 )->textContent ;
		$char_code = $item->getElementsByTagName( 'ISO_Char_Code' )->item( 0 )->textContent ;
		$num_code = $item->getElementsByTagName( 'ISO_Num_Code' )->item( 0 )->textContent ;
		$nominal = $item->getElementsByTagName( 'Nominal' )->item( 0 )->textContent ;
		$parent_id = $item->getElementsByTagName( 'ParentCode' )->item( 0 )->textContent ;

		$sth_ins->execute( [
			':id' => $id ,
			':name' => $name ,
			':char_code' => $char_code ,
			':num_code' => sprintf( '%03d' , $num_code ) ,
			':nominal' => $nominal ,
			':parent_id' => $parent_id ,
		] ) ;
	}

	$sth_ins->closeCursor( ) ;

	$sth_ins = $dbh->prepare( "
INSERT IGNORE INTO
	`rate`
SET
	`currency_id` := :currency_id ,
	`date` := :date ,
	`nominal` := :nominal ,
	`value` := :value ;
	" ) ;

	$sth_sel = $dbh->prepare( "
SELECT
	`c1`.`id` AS `currency_id` ,
	date_format( adddate( current_date( ) , INTERVAL -1 MONTH ) , '%d/%m/%Y' ) AS `date_start` ,
	date_format( current_date( ) , '%d/%m/%Y' ) AS `date_finish`
FROM
	`currency` AS `c1` ;
	" ) ;
	$sth_sel->execute( ) ;

	while ( list( $currency_id , $date_start , $date_finish ) = $sth_sel->fetch( \PDO::FETCH_NUM ) ) {
		foreach ( self::getDocument( 'http://www.cbr.ru/scripts/XML_dynamic.asp?' .
				implode( '&' , [
					'date_req1=' . $date_start ,
					'date_req2=' . $date_finish ,
					'VAL_NM_RQ=' . $currency_id ,
				] )
			)->query( '/ValCurs/Record' ) as $item ) {
			$date = $item->getAttribute( 'Date' ) ;
			$nominal = $item->getElementsByTagName( 'Nominal' )->item( 0 )->textContent ;
			$value = $item->getElementsByTagName( 'Value' )->item( 0 )->textContent ;

			$date = preg_replace( '{(\d\d)\.(\d\d)\.(\d\d\d\d)}' , '$3-$2-$1' , $date ) ;
			$value = implode( '.' , explode( ',' , $value ) ) ;

			$sth_ins->execute( [
				':currency_id' => $currency_id ,
				':date' => $date ,
				':nominal' => $nominal ,
				':value' => $value ,
			] ) ;
		}
	}

	$sth_sel->closeCursor( ) ;
	$sth_ins->closeCursor( ) ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
DELETE
	`r1`.*
FROM
	`rate` AS `r1` ;
	" ) ;

	$dbh->exec( "
DELETE
	`c1`.*
FROM
	`currency` AS `c1` ;
	" ) ;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_091134_currency_rate_seed cannot be reverted.\n";

        return false;
    }
    */
}
