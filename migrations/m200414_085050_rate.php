<?php

use yii\db\Migration;

/**
 * Class m200414_085050_rate
 */
class m200414_085050_rate extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `currency`(
	`id` CHAR( 6 ) NOT null COMMENT 'идентификатор' ,
	`name` VARCHAR( 60 ) NOT null COMMENT 'название' ,
	`num_code` CHAR( 3 ) NOT null COMMENT 'числовой код валюты' ,
	`char_code` CHAR( 3 ) NOT null COMMENT 'буквенный код валюты' ,
	`nominal` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'номинал' ,
	`parent_id` CHAR( 6 ) NOT null COMMENT 'родитель' ,

	PRIMARY KEY( `id` ) ,
	UNIQUE( `char_code` ) ,
	UNIQUE( `num_code` )
) COMMENT 'валюта' ;
	" ) ;
	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `rate`(
	`currency_id` CHAR( 6 ) NOT null COMMENT 'идентификатор валюты' ,
	`date` DATE NOT null COMMENT 'дата' ,
	`nominal` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'номинал' ,
	`value` DECIMAL( 11 , 4 ) UNSIGNED NOT null COMMENT 'отношение к USD' ,

	PRIMARY KEY( `currency_id` , `date` ) ,
	CONSTRAINT `fk_rate_currency_currency_id`
		FOREIGN KEY ( `currency_id` )
		REFERENCES `currency`( `id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
) COMMENT 'курс валюты' ;
	" ) ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
DROP TABLE IF EXISTS `rate` ;
	" ) ;

	$dbh->exec( "
DROP TABLE IF EXISTS `currency` ;
	" ) ;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200414_085050_rate cannot be reverted.\n";

        return false;
    }
    */
}
