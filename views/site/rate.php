<?php
	$this->title = 'Котировки валют' ;
?>
<h1><?=htmlspecialchars( $this->title )?></h1>
<form>
	<fieldset>
		<legend>поиск</legend>
		<label>
			<select name="valueID">
				<option value="">Валюта</option>
				<?php foreach ( $currencyes as $currency ) { ?>
				<option value="<?=htmlspecialchars( $currency->id )?>"
					<?php if ( $currency->id == $valueID ) { ?>
						selected
					<?php } ?>
				><?=htmlspecialchars( $currency->char_code )?></option>
				<?php } ?>
			</select>
		</label>
		<label>
			<span>дата с:</span>
			<input type="date" name="from" value="<?=htmlspecialchars( $from )?>">
		</label>
		<label>
			<span>дата по:</span>
			<input type="date" name="to" value="<?=htmlspecialchars( $to )?>">
		</label>
		<label>
			<span>найти</span>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>
<table class="table">
	<thead>
		<tr>
			<th>Код</th>
			<th>Дата</th>
			<th>Номинал</th>
			<th>Котировка</th>
			<th>Название</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $rates as $rate ) { ?>
		<tr>
			<td><?=htmlspecialchars( $rate[ 'currency_char_code' ] )?></td>
			<td><?=htmlspecialchars( $rate[ 'rate_date' ] )?></td>
			<td><?=htmlspecialchars( $rate[ 'rate_nominal' ] )?></td>
			<td><?=htmlspecialchars( $rate[ 'rate_value' ] )?></td>
			<td><?=htmlspecialchars( $rate[ 'currency_name' ] )?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>