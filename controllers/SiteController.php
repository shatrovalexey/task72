<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Currency;
use app\models\Rate;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors( ) {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions( ) {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function beforeAction( $action ) {
		if (
			! in_array( $action->actionMethod , [ 'actionIndex' ] ) &&
			( \Yii::$app->user->isGuest ) ||
			! parent::beforeAction( $action )
		) {
			return $this->goHome( ) ;
		}

		return true ;
	}

	/**
	* Главная страница
	*
	* @param string $valueID - идентификатор валюты
	* @param date $from - дата с
	* @param date $from - дата по
	*
	* @return string
	*/
	public function actionIndex( $valueID = null , $from = null , $to = null ) {
		if ( \Yii::$app->user->isGuest ) {
			return $this->actionLogin( ) ;
		}

		$rate = $this->getRate( $valueID , $from , $to ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $rate->count( ) ,
			'pageSize' => 10 ,
		] ) ;

		$rates = $rate->offset( $pages->offset )->limit( $pages->limit ) ;

		return $this->render( 'rate' , [
			'currencyes' => Currency::find( )
				->orderBy( [ 'char_code' => SORT_ASC , ] )->all( ) ,
			'pages' => $pages ,
			'rates' => $rates->asArray( )->all( ) ,
			'valueID' => $valueID ,
			'from' => $from ,
			'to' => $to ,
		] ) ;
	}

	/**
	* Метод API для получения котировок
	*
	* @param string $valueID - идентификатор валюты
	* @param date $from - дата с
	* @param date $from - дата по
	*
	* @return string
	*/
	public function actionRate( $valueID = null , $from = null , $to = null ) {
		$rate = $this->getRate( $valueID , $from , $to ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $rate->count( ) ,
			'pageSize' => 10 ,
		] ) ;

		$rates = $rate->offset( $pages->offset )->limit( $pages->limit ) ;

		return $this->asJson( [
			'rates' => $rates->asArray( )->all( ) ,
			'pages' => [
				'totalCount' => $pages->totalCount ,
				'pageSize' => $pages->pageSize ,
				'offset' => $pages->offset ,
				'limit' => $pages->limit ,
			] ,
		] ) ;
	}

	/**
	* Данные по котировкам
	*
	* @param string $valueID - идентификатор валюты
	* @param date $from - дата с
	* @param date $from - дата по
	*
	* @return yii\db\Query
	*/
	protected function getRate( $valueID = null , $from = null , $to = null ) {
		$where = [ ] ;
		$args = [ ] ;

		if ( ! empty( $valueID ) ) {
			$where[] = 'currency.id = :valueID' ;
			$args[ ':valueID' ] = $valueID ;
		}
		if ( ! empty( $from ) ) {
			$where[] = 'rate.date >= :from' ;
			$args[ ':from' ] = $from ;
		}
		if ( ! empty( $to ) ) {
			$where[] = 'rate.date <= :to' ;
			$args[ ':to' ] = $to ;
		}

		$query = Currency::find( )
			->select( [
				'currency.name AS currency_name' ,
				'currency.char_code AS currency_char_code' ,
				'rate.nominal AS rate_nominal' ,
				'rate.value AS rate_value' ,
				'rate.date AS rate_date' ,
			] )
			->innerJoin( 'rate' , 'currency.id = rate.currency_id' )
			->orderBy( [ 'rate.date' => SORT_DESC , ] ) ;

		if ( $where ) {
			$query->where( implode( ' AND ' , $where ) , $args ) ;
		}

		return $query ;
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
