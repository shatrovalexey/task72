<?php

namespace app\models ;

use Yii ;
use yii\db\ActiveRecord ;

/**
* Валюта
*/
class Currency extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'currency' ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'name' , 'num_code' , 'char_code' , 'nominal' , 'parent_id' , ] , 'required' ] ,
			[ [ 'nominal' , ] , 'integer' ] ,
			[ [ 'num_code' , 'char_code' ] , 'string' , 'length' => 3 , 'skipOnEmpty' => false , ] ,
			[ [ 'num_code' , ] , 'integer' , ] ,
			[ [ 'parent_id' , ] , 'exist' , 'skipOnError' => true ,
				'targetClass' => static::className( ) ,
				'targetAttribute' => [ 'parent_id' => 'id' , ] ,
			] ,
		] ;
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'id' => 'идентификатор' ,
			'name' => 'название' ,
			'nominal' => 'номинал' ,
			'char_code' => 'буквенный код ISO' ,
			'num_code' => 'числовой код ISO' ,
			'parent_id' => 'родитель' ,
		] ;
	}
}
