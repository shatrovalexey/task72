<?php

namespace app\models ;

use Yii ;
use yii\db\ActiveRecord ;
use app\models\Currency ;

/**
* Курс валюты
*/
class Rate extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'rate' ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'currency_id' , 'date' , 'nominal' , 'value' ] , 'required' ] ,
			[ [ 'nominal' , ] , 'integer' ] ,
			[ [ 'date' , ] , 'date' , 'format' => 'php:Y-m-d' ] ,
			[ [ 'value' , ] , 'double' , ] ,
			[ [ 'currency_id' , 'parent_id' , ] , 'string' , 'length' => 6 , ] ,
			[ [ 'currency_id' , ] , 'exist' , 'skipOnError' => true ,
				'targetClass' => Currency::className( ) ,
				'targetAttribute' => [ 'currency_id' => 'id' , ] ,
			] , ,
		] ;
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'currency_id' => 'идентификатор валюты' ,
			'date' => 'дата' ,
			'nominal' => 'номинал' ,
			'value' => 'значение' ,
		] ;
	}
}
